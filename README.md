![Noggit Red](/assets/noggit_icon_64.png) **Noggit Red Build**

---
# Noggit Red Build

This repository contains the docker file required to build the Noggit Red build image. Upon build, the docker image is added to the GitLab container registry as [`registry.gitlab.com/prophecy-rp/noggit-red-build:latest`](https://gitlab.com/prophecy-rp/noggit-red-build/container_registry/2447582).

---
## Build Variables

There are several build variables set up for this project which contain how the build script works, the table below outlines these. 

| Variable  | Description |
| ----------- | ----------- |
| CI_REGISTRY_IMAGE    | The name given to the image we publish. 
| CI_DOCKERFILE_NAME   | The name of the dockerfile we build.   
| CI_REGISTRY_USER     | The username for the container registry.
| CI_REGISTRY_PASSWORD | The password for the container registry.
| CI_REGISTRY          | The container registry url determines where the images is published.
| CI_COMMIT_BRANCH     | The name of the branch we build.
---
## Building

You can build the image locally using the following docker commands.

```powershell
docker build -t noggit-red-build:latest -f .\Dockerfile-Noggit-Red --no-cache .
```

